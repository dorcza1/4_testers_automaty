from src.car_warranty import check_if_car_has_warranty

def test_car_produced_in_2020_with_30000_mileage():
    assert check_if_car_has_warranty(2020, 30000) == True

def test_car_produced_in_2020_with_70000_mileage():
    assert check_if_car_has_warranty(2020, 70000) == False

def test_car_produced_in_2016_with_30000_mileage():
    assert check_if_car_has_warranty(2016, 30000) == False

def test_car_produced_in_2016_with_120000_mileage():
    assert check_if_car_has_warranty(2016, 120000) == False

#pobieranie danych ze słownika za pomocą **
new_car_low_mileage = {
    "production_year": 2023,
    "mileage": 10000
}

def test_new_car_with_low_mileage():
    assert check_if_car_has_warranty(**new_car_low_mileage) == True