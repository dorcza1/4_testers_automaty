import src.employee_mamagement_from_classes
from assertpy import assert_that

def test_generating_random_email():
    employee_dict = src.employee_mamagement_from_classes.generate_employee_dictionary()
    generated_email = employee_dict["email"]
    assert generated_email.endswith("@example.com")

def test_generating_random_seniority_years():
    employee_dict = src.employee_mamagement_from_classes.generate_employee_dictionary()
    generated_seniority = employee_dict["seniority_years"]
    assert 0 <= generated_seniority <= 40


def test_generating_employee_data():
    assert "email" in src.employee_mamagement_from_classes.generate_employee_dictionary()
    assert "seniority_years" in src.employee_mamagement_from_classes.generate_employee_dictionary()
    assert "female" in src.employee_mamagement_from_classes.generate_employee_dictionary()


def test_two_generated_eployee_dictionaries_are_different():
    employee_1 = src.employee_mamagement_from_classes.generate_employee_dictionary()
    employee_2 = src.employee_mamagement_from_classes.generate_employee_dictionary()
    assert employee_1 != employee_2
    assert assert_that(employee_1).is_not_equal_to(employee_2)

def test_if_eployee_list_has_correct_length():
    employees = src.employee_mamagement_from_classes.generate_list_of_employees(3)
    assert len(employees) == 3
def test_filter_list_of_employees_by_seniority_greater_than():
    employees = [
        {"email": "a@example.com", "seniority_years": 10, "female": True},
        {"email": "b@example.com", "seniority_years": 5, "female": False},
        {"email": "c@example.com", "seniority_years": 15, "female": True},
    ]
    senior_employees = src.employee_mamagement_from_classes.filter_list_of_employees_by_seniority_greater_than(employees, 10)
    assert len(senior_employees) == 1

def test_filter_list_of_employees_for_females():
    employees = [
        {"email": "a@example.com", "seniority_years": 10, "female": True},
        {"email": "b@example.com", "seniority_years": 5, "female": False},
        {"email": "c@example.com", "seniority_years": 15, "female": True},
    ]
    female_employees = src.employee_mamagement_from_classes.filter_list_of_employees_by_gender(employees, female=True)
    assert len(female_employees) == 2

def test_filter_list_of_only_male_employees_for_females():
    employees = [
        {"email": "a@example.com", "seniority_years": 10, "female": False},
        {"email": "b@example.com", "seniority_years": 5, "female": False},
        {"email": "c@example.com", "seniority_years": 15, "female": False},
    ]
    female_employees = src.employee_mamagement_from_classes.filter_list_of_employees_by_gender(employees,female=True)
    assert len(female_employees) == 0
def test_filter_list_of_only_female_employees_for_females():
    employees = [
        {"email": "a@example.com", "seniority_years": 10, "female": True},
        {"email": "b@example.com", "seniority_years": 5, "female": True},
        {"email": "c@example.com", "seniority_years": 15, "female": True},
    ]
    female_employees = src.employee_mamagement_from_classes.filter_list_of_employees_by_gender(employees, female=True)
    assert len(female_employees) == 3