from src.homework2 import check_if_animal_requires_vaccination
def test_cat_age_one_requires_vaccination():
    assert check_if_animal_requires_vaccination("cat", 1) == True

def test_dog_age_one_requires_vaccination():
    assert check_if_animal_requires_vaccination("dog", 1) == True

def test_dog_age_three_requires_vaccination():
    assert check_if_animal_requires_vaccination("dog", 3) == False