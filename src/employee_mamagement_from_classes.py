import random
import uuid


def generate_employee_dictionary():
    return {
        "email": f"{uuid.uuid4()}@example.com",
        "seniority_years": random.randint(0,40),
        "female": bool(random.randint(0,1)),
    }

def generate_list_of_employees(list_length):
    # employees = []
    # for i in range(list_length):
    #     employees.append(generate_employee_dictionary())
    # return employees
    return [generate_employee_dictionary() for i in range(10)] #list comprehention

def filter_list_of_employees_by_seniority_greater_than(employees, seniority_years):
    senior_employees = []
    for employee in employees:
        if employee["seniority_years"] > seniority_years:
            senior_employees.append(employee["email"])
    return senior_employees


def filter_list_of_employees_by_gender(employees, female=True):
    # output = []
    # for employee in employees:
    #     if employee["female"] == female:
    #         output.append(employee)
    # return output
    return [employee for employee in employees if employee["female"] == female]

if __name__ == '__main__':
    employees = generate_list_of_employees(10)
    print(employees)
    senior_employees = filter_list_of_employees_by_seniority_greater_than(employees, 10)
    print(senior_employees)
    female_employees = filter_list_of_employees_by_gender(employees)
    print(female_employees)

