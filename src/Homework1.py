print("--------")
print("task #1")
print("--------")
#Dla listy:
#Wydrukuj:
# długość listy
# pierwszy element
# ostatni element
# Dodaj nowy email ‘cde@example.com’

emails = ["a@example.com", "b@example.com"]
print(len(emails))
print(emails[0])
print(emails[-1])
emails.append("cde@example.com")
print(emails)

print("--------")
print("task #2")
print("--------")
#Napisz funkcję, która będzie generować adresy email w domenie naszej firmy -> “4testers.pl”.
#Email ma formę “imie.nazwisko@4testers.pl”
#Niech funkcja przyjmuje imię i nazwisko i zwraca utworzony email.
#Wydrukuj emaile dla użytkowników: Janusz Nowak Barbara Kowalska

def create_emails_for_users(surname,last_name):
    print (surname.lower()+"."+last_name.lower()+"@4testers.pl")

create_emails_for_users("Janusz", "Nowak")
create_emails_for_users("Barbara", "Kowalska")

print("--------")
print("task #3")
print("--------")
#Napisz funkcję, która przyjmuje listę ocen ucznia i zwraca jej średnią.

def calculate_students_average(students_name, list_of_grades):
     print(f"{students_name}'s average is: {sum(list_of_grades)/len(list_of_grades)}")

calculate_students_average("Anna",[5,3,5,4,5,5])
calculate_students_average("Tom",[4,3,2,5,3,4])

print("---------------------")
print("rozwiązanie z zajęć:")
print("---------------------")

def describe_list_of_emails(emails):
    print(f"First element of the list is: {emails[0]}")
    print(f"last element of the list is: {emails[-1]}")
    emails.append("das@example.com")
    print(f"Currently the list is: {emails}")
def create_email_in_domain_4testers(first_name, last_name):
    return f"{first_name.lower()}.{last_name.lower()}@4testers.pl"

def get_average_grade(list_of_grades):
    return sum(list_of_grades)/len(list_of_grades)

def create_email_for_users_in_domain(prefix, domain="gmail.com"):
    return f"{prefix}@{domain}"

def generate_user_contact_dictionary(email, phone_number, country="Poland", country_code="+48"):
    return {
        "email":email,
        "phone_number":f"{country_code}{phone_number}",
        "country": country
    }
if __name__ == '__main__':
    describe_list_of_emails(emails)

    print(create_email_in_domain_4testers("Janusz", "Nowak"))
    print(create_email_in_domain_4testers("Barbara", "Kowalska"))

    grades = [5,4,4,2,3,5,2,4,3,3]
    print(get_average_grade(grades))

    print(create_email_for_users_in_domain("adrian123", "onet.pl"))
    print(create_email_for_users_in_domain("adrian123"))

    print(generate_user_contact_dictionary("dorota123@o2.pl", 666444666))
    print(generate_user_contact_dictionary("dorota123@o2.pl", 666444666, country_code="+42"))
