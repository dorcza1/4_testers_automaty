
def get_three_highest_number_in_the_list(list_of_numbers):
    sorted_list = sorted(list_of_numbers)
    return sorted_list[-3:]


if __name__ == '__main__':
    movies = ["film1", "film2", "film3", "film4", "film5" ]

print(len(movies))
print(movies[0])
last_movie_index = len(movies) - 1
print(movies [-1])

movies.append(("film6"))
movies.insert(2, "film7")
print(movies)

print(get_three_highest_number_in_the_list([1, 8, -86, 13, 100, -55]))
print(get_three_highest_number_in_the_list(["a", "c", "f", "z", "p"]))
print(get_three_highest_number_in_the_list(["a", "c", "f", "z", 10])) #będzie błąd - nie można mieszać typów