pet = {
    "name" : "Sonia",
    "kind" : "dog",
    "age": 13,
    "weight" : 35,
    "is_male" : False,
    "Favourite_food" : ["kabanos", "fish"]
}
print(pet["weight"])
pet["weight"] = 40
print(pet["weight"])
pet["likes_swimming"] = True
print(pet)
del pet["is_male"]
print(pet)
pet["Favourite_food"].append("smoked_bones")
print(pet)

def set_person_data(email, phone, city, street):
    return {
        'contact': {
            'email': email,
            'phone': phone
        },
        'address': {
            'city': city,
            'street': street
        }
    }

# Example usage:
person_data = set_person_data("xxx@xxx.com", "+48666444666", "Wrocław", "Kościuszki")
print(person_data)
