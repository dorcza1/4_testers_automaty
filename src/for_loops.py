import random
def print_ten_numbers():
    for i in range(10): #wykonuje 10 razy
        print(i)

def print_even_numers_in_rangre():
    for i in range(0, 21, 2):
        print(i)

def print_numbers_divisible_by_seven():
    for i in range(7, 31, 7):
        print(i)
def print_numbers_from_one_to_thirty_divisible_by_seven():
    for i in range(1, 31):
        if i % 7 == 0:
            print(i)
def print_random_numbers(n):
    for i in range(n):
      print(random.randint(1, 1000))

def generate_list_of_10_random_number_from_1000_to_5000():
    numbers = []
    for i in range(10):
        numbers.append(random.randint(1000, 5000))
    return numbers

def print_each_number_in_the_list_squared(input_list):
    for number in input_list:
        print(number**2)
def convert_C_to_F(temeratures_in_C):
    temperatures_in_F = []
    for temp in temeratures_in_C:
        return temperatures_in_F.append(temp * 9/5 + 32)

def convert_celsius_to_fahrenheit(temp_in_celcius):
    return temp_in_celcius * 9/5 +32

#MAPOWANIE
#Tomka:
def change_celcius_to_fahrenheit(list_of_tempreture_in_celcius: list):
    list_of_temperature_in_fahrenheit = []
    for temperature in list_of_tempreture_in_celcius:
        list_of_temperature_in_fahrenheit.append((temperature * 1.8) + 32)
    return list_of_temperature_in_fahrenheit


#FILTROWANIE
def filter_grades_greater_or_equal_3(grades):
    accaptable_grades = []
    for grade in grades:
        if grade >= 3:
            accaptable_grades.append(grade)
    return accaptable_grades

if __name__ == '__main__':
    print_ten_numbers()
    print("-------------------------------------------------------")

    print_numbers_from_one_to_thirty_divisible_by_seven()
    print("-------------------------------------------------------")

    print(generate_list_of_10_random_number_from_1000_to_5000())
    print("-------------------------------------------------------")

    numbers = [2,6,7,3,15]
    print_each_number_in_the_list_squared(numbers)
    print("-------------------------------------------------------")
    temp_in_C = [10.3, 23.2, 15.8, 19.0, 14.0, 23.0, 25.0]
    convert_C_to_F(temp_in_C)
    print("-------------------------------------------------------")

    list_of_tempreture_in_celcius = [10.3, 23.2, 15.8, 19.0, 14.0, 23.0, 25.0]
    print(change_celcius_to_fahrenheit(list_of_tempreture_in_celcius))
    print("-------------------------------------------------------")

    # 10 randomowych liczb od 1 do 1000
    print_random_numbers(10)
    print("-------------------------------------------------------")

    temperatures_in_C = [10.3, 23.2, 15.8, 19.0, 14.0, 23.0, 25.0]
    print(change_celcius_to_fahrenheit(temperatures_in_C))
    print("-------------------------------------------------------")

    print(filter_grades_greater_or_equal_3([3,4,6,5,2,4,3,2,3,5]))
