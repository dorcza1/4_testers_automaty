animals = [
    {
        "name": "Pusia",
        "kind": "cat",
        "age": 2,
        "male": False
    },
    {
        "name": "Azor",
        "kind": "dog",
        "age": 5,
        "male": True
    },
    {
        "name": "Tuptuś",
        "kind": "turtle",
        "age": 40,
        "male": True
    },
]
print(animals)
print(animals[0])
last_animal = animals[-1]
last_animals_age = last_animal["age"]
print("The age of the last animal: ", last_animals_age)
print(animals[0]["male"])
print(animals[1]["kind"])

animals.append({
        "name":"Pasiak",
        "kind": "zebra",
        "age":10,
        "male": True
    })
print(animals)