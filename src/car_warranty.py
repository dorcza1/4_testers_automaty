import datetime
# Napisz funkcję, która przyjmie rocznik i przebieg samochodu i zwróci czy samochód
# nadal ma gwarancję.
# Jeśli wiek samochodu jest większy niż 5 lat lub przebieg jest większy niż 60 000
# kilometrów - > funkcja zwraca False. W przeciwnym wypadku zwraca True.
# Przetestuj funkcję dla samochodów z danymi:
# rocznik: 2020, przebieg 30 000
# rocznik: 2020, przebieg 70 000
# rocznik: 2016, przebieg 30 000
# rocznik: 2016, przebieg 120 000

def check_if_car_has_warranty(production_year, mileage):
    if (datetime.datetime.now().year - production_year)>5 or mileage >60000:
        return False
    else:
        return True

if __name__ == '__main__':
    print("rocznik: 2020, przebieg 30 000 ma gwarancję:",check_if_car_has_warranty(2020, 30000))
    print("rocznik: 2020, przebieg 70 000 ma gwarancję:",check_if_car_has_warranty(2020, 70000))
    print("rocznik: 2016, przebieg 30 000 ma gwarancję:",check_if_car_has_warranty(2016, 30000))
    print("rocznik: 2016, przebieg 120 000 ma gwarancję:",check_if_car_has_warranty(2016, 120000))


