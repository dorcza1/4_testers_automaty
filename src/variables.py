friends_name = "Kasia"
friends_age = 39
friends_number_of_pets = 1
has_driving_license = False
friendship_years = 27.5

print("Name:", friends_name, sep="\n") # \t separacja tabulator
print("Age:", friends_age, sep="\n") #separacja nowa linia
print("Pet amount:", friends_number_of_pets, sep="\n")
print("Driving license:", has_driving_license, sep="\n")
print("Friendship years:", friendship_years, sep="\n")

print("--------------------")

# prostszy sposób
print(
    "Name:", friends_name,
    "Age:", friends_age,
    "Pet amount:", friends_number_of_pets,
    "Driving license:", has_driving_license,
    "Friendship years:", friendship_years,
    sep="\n"
)

name_surname = "Dorota Dobrzyńska"
email = "dorota@o2.pl"
mobile = "+48 123 123 123"
print("********************************************")

bio = "Name: "+ name_surname + "\nEmail: " + email + "\nMobile: " + mobile
print(bio)
print("********************************************")

bio_smarter = f"Name: {name_surname}\nEmail: {email}\nMobile: {mobile}"
print(bio_smarter)
print("********************************************")

bio_docstring = f"""Name: {name_surname}
Email: {email}
Mobile: {mobile}"""

print(bio_docstring)