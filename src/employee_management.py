import random
from typing import List, Any

from faker import Faker

fake = Faker()
def generate_random_email():
    return fake.email()

def generate_random_number():
    return random.randint(0, 40)

def generate_random_boolean():
    return random.choice([True, False])

def create_employee_dict():
    random_email = generate_random_email()
    seniority_years = generate_random_number()
    is_female = generate_random_boolean()
    return {
        "email": random_email,
        "seniority_years": seniority_years,
        "female": is_female
    }
#Napisz funkcję, która zwróci listę słowników z poprzedniego
# zadania o żądanej długości. Argumentem funkcji ma być ilość
# słowników w zwracanej liście.
# W bloku main wygeneruj listę 10 słowników z użyciem tej funkcji

def generate_list_of_eployees(list_lenght):
    list_of_employees = []
    for number in range(list_lenght):
        list_of_employees.append(create_employee_dict())
    return(list_of_employees)

# Napisz funkcję, która przyjmie listę słowników wygenerowaną
# poprzednio i zwróci wszystkie emaile pracowników, którzy mają
# staż pracy (klucz seniority_years) większy niż 10.
# Napisz funkcję, która przyjmie listę słowników zdefiniowaną
# poprzednio i zwróci wszystkie słowniki kobiet (“female”: True)
def get_emails_of_employees_with_seniority_years_greater_than_10():
    pass


if __name__ == '__main__':
    print(create_employee_dict())
    print(generate_list_of_eployees(10))
    print(get_emails_of_employees_with_seniority_years_greater_than_10())