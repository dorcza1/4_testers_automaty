print("--------")
print("task #1")
print("--------")
#Utwórz funkcję, która przyjmuje słownik opisujący gracza:
gamer1 = {
  "nick": "maestro_54",
  "type": "warrior",
  "exp_points": 3000
}
# i zwraca jego opis w postaci:
# “The player maestro_54 is of type warrior and has 3000 EXP”

def create_gamers_bio(player1):
    print(f"The player {player1["nick"]} is of type {player1["type"]} and has {player1["exp_points"]} EXP")

create_gamers_bio(gamer1)

print("--------")
print("task #2")
print("--------")
# Napisz funkcję, która przyjmuje dwa parametry:
# rodzaj zwierzęcia
# wiek
# Funkcja zwraca informację czy zwierzę wymaga szczepienia.
# Wszystkie zwierzęta wymagają szczepienia w pierwszym roku życia.
# Zwierzęta wymagają ponownego szczepienia:
# koty co 3 lata
# psy co 2 lata
# Stestuj funkcję dla różnych przypadków.

def vaccination_calculator(animal_kind, animal_age):
    if animal_kind == "dog" and animal_age == 1:
        print(f"Vaccinate your {animal_kind}")
    elif animal_kind == "cat" and animal_age == 1:
        print(f"Vaccinate your {animal_kind}")
    elif (animal_kind != "cat" or animal_kind != "dog") and animal_age == 1:
        print(f"Vaccinate your {animal_kind}")
    elif animal_kind == "dog" and animal_age in [3,5,7,9,11,13,15,17,19,21]:
        print(f"Vaccinate your {animal_kind}")
    elif animal_kind == "cat" and animal_age in [4,7,10, 13,16,19,22,25]:
        print(f"Vaccinate your {animal_kind}")
    else:
        print(f"No vaccination required")

vaccination_calculator('dog', 1)
vaccination_calculator('cat', 2)
vaccination_calculator('cat', 7)
vaccination_calculator('hamster', 1)

print("---------------------")
print("rozwiązanie z zajęć:")
print("---------------------")

def print_player_description(player_dict: dict):
    source = player_dict.get("source", "Steam") #pobiera wartość klucza ze słownika, a jaęzli go nie ma to wypełnia słowem "steam"
    print(f"The player {player_dict["nick"]} is of type {player_dict["type"]} and has {player_dict["exp_points"]} EXP")
    print(f"The source of player is {source}")

def check_if_animal_requires_vaccination(kind, age):
    if age <= 0:
        raise ValueError ("Age must be grater than or equal to zero")
    elif kind not in ["dog", "cat"]:
        raise ValueError ("Only cats and dogs are supported.")
    # if age == 1 or (kind == "dog" and (age - 1)% 2 == 0) or (kind == "cat" and (age - 1)% 3 == 0)
    #     return True
    # else:
    #     return False
# daje to samo:
    return (kind == "dog" and (age - 1)% 2 == 0) or (kind == "cat" and (age - 1)% 3 == 0)

if __name__ == '__main__':
    game_player_1 = {
        "nick": "maestro_54",
        "type": "warrior",
        "exp_points": 3000
    }
    game_player_2 = {
        "nick": "kolo_365",
        "type": "magician",
        "exp_points": 2000,
        "source" : "Playstation"
    }

    print_player_description(game_player_1)
    print_player_description(game_player_2)

    print("cat, 1,", check_if_animal_requires_vaccination("cat", 1))
    print("dog, 1,", check_if_animal_requires_vaccination("dog", 1))
    print("cat, 2,", check_if_animal_requires_vaccination("cat", 2))
    print("cat, 4,", check_if_animal_requires_vaccination("cat", 4))
    print("dog, 3,", check_if_animal_requires_vaccination("dog", 3))
    print("dog, 2,", check_if_animal_requires_vaccination("dog", 2))
    #print("dog, -1,", check_if_anima_requires_vaccination("dog",-1))
    #print("mouse, 2,", check_if_anima_requires_vaccination("mouse",2))
