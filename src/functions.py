def print_area_of_a_circle_with_radius(r):
    area_of_a_circle = 3.1415 * r ** 2
    print(area_of_a_circle)

def upper_word(word: str):  # definiowanie funkcji
    return word.upper()

def add_two_numbers(a, b):
    return a + b

def square(a):
    return a ** 2
    print (square)

def compute_square_of_number(number):
    return number ** 2

def calculate_cubid_volume(a,b,c):
    return a * b * c

def convert_celsius_to_ferenheit(temp_in_C):
    return temp_in_C * 9 / 5 + 32

def print_greeting(name, city):
    return print(f"Witaj {name}! Miło Cię widzieć w naszym mieście: {city}!")

if __name__ == '__main__':
    print_area_of_a_circle_with_radius(5)

    big_dog = upper_word("dog")  # przypisujemy jeżeli będziemy używać więcej niż raz
    print(big_dog)
    print(upper_word("cat"))

    print(add_two_numbers("kot", "pies"))
    print(add_two_numbers(3, 5))
    print(add_two_numbers(False, True))  # T to 1, F to 0
    # print(add_two_numbers(3,"pies")) #wyjdzie błąd
    print(add_two_numbers(3, True))

    print (square (0))
    print (square (16))
    print (square (2.55))

    print(calculate_cubid_volume(3,5,7))

    print(convert_celsius_to_ferenheit(20))

    print_greeting("Michał", "Toruń")
    print_greeting("Beata", "Gdynia")
