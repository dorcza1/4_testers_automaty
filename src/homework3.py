import random
import datetime

# Listy zawierające dane do losowania
female_fnames = ['Kate', 'Agnieszka', 'Anna', 'Maria', 'Joss', 'Eryka']
male_fnames = ['James', 'Bob', 'Jan', 'Hans', 'Orestes', 'Saturnin']
surnames = ['Smith', 'Kowalski', 'Yu', 'Bona', 'Muster', 'Skinner', 'Cox', 'Brick', 'Malina']
countries = ['Poland', 'United Kingdom', 'Germany', 'France', 'Other']

def generate_random_female_first_name():
    return random.choice(female_fnames)

def generate_random_male_first_name():
    return random.choice(male_fnames)

def generate_random_surname():
    return random.choice(surnames)

def generate_random_age():
    return random.randint(5, 45)

def generate_random_country():
    return random.choice(countries)


def is_adult(age):
    if age >= 18:
        return True
    else:
        return False

def generate_random_female():
    first_name = generate_random_female_first_name()
    surname = generate_random_surname()
    age = generate_random_age()
    return {
    'firstname': first_name,
    'lastname': surname,
    'email': f"{first_name}.{surname}@example.com".lower(),
    'age': age,
    'country': generate_random_country(),
    'adult': is_adult(age),
    'birth_year': datetime.datetime.now().year-age
}

def generate_random_male():
    first_name = generate_random_male_first_name()
    surname = generate_random_surname()
    age = generate_random_age()
    return {
    'firstname': first_name,
    'lastname': surname,
    'email': f"{first_name}.{surname}@example.com".lower(),
    'age': age,
    'country': generate_random_country(),
    'adult': is_adult(age),
    'birth_year': datetime.datetime.now().year-age
}
def generate_list_of_5_females_and_5_males():
    list_of_people = []
    for i in range(5):
        list_of_people.append(generate_random_female())
    for i in range(5):
        list_of_people.append(generate_random_male())
    return list_of_people

def generate_persons_description():
    list_of_people = generate_list_of_5_females_and_5_males()
    for person in list_of_people:
        print (f"Hi! I'm {person["firstname"]} {person["lastname"]}. I come from {person["country"]} and I was born in {person["birth_year"]}.")


if __name__ == '__main__':
    # print(generate_random_female_first_name())
    # print(generate_random_male_first_name())
    # print(generate_random_surname())
    # print(generate_random_age())
    # print(generate_random_country())
    # print(generate_random_female())
    # print(generate_random_male())
    print(generate_list_of_5_females_and_5_males())
    list_of_people = generate_list_of_5_females_and_5_males()
    print(len(list_of_people))
    print(generate_persons_description())