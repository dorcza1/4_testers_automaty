import datetime

class Car:
    def __init__(self, brand, color, production_year):
        self.brand = brand
        self.color = color
        self.production_year = production_year
        self.mileage = 0

    def drive(self, distance):
        self.mileage += distance

    def get_age(self):
        current_year = datetime.datetime.now().year
        age = current_year - self.production_year
        return age

    def repaint(self, color):
        self.color = color

if __name__ == '__main__':
    car1 = Car("Ferrari", "Red", 2020)
    car2 = Car("Mustang", "Black", 2010)
    car3 = Car("Porshe", "Yellow", 2024)

    print("Car1:")
    print(car1.brand)
    print(car1.color)
    print(car1.production_year)
    print(car1.mileage)
    print(car1.get_age())

    print("Car2:")
    print(car2.brand)
    print(car2.color)
    print(car2.production_year)
    print(car2.mileage)
    print(car2.get_age())


    print("Car3:")
    print(car3.brand)
    print(car3.color)
    print(car3.production_year)
    print(car3.mileage)
    print(car3.get_age())


    car1.drive(10000)
    print("Car1:")
    print(car1.brand)
    print(car1.color)
    print(car1.production_year)
    print(car1.mileage)

    car2.repaint("Green")
    print("Car2:")
    print(car2.brand)
    print(car2.color)
    print(car2.production_year)
    print(car2.mileage)

    car3.drive(50000)
    car3.repaint("silver")
    print("Car3:")
    print(car3.brand)
    print(car3.color)
    print(car3.production_year)
    print(car3.mileage)


