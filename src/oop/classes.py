class Dog:
    def __init__(self, name, ):
        self.name = name
        self.age = 0
        self.breed = "mixed"

    def get_name(self):
        return self.name

    def is_puppy(self):
        return self.age < 1

    def set_breed(self, new_breed):
        self.breed = new_breed

if __name__ == '__main__':
    small_dog = Dog("Burek",)
    big_dog = Dog("Cerberus",)
    medium_dog = Dog("Ciapek", )

    print(small_dog.name)
    print(big_dog.name)
    print((small_dog.get_name()))
    print(big_dog.get_name())
    print(small_dog.age)
    small_dog.set_breed("Chihuaua")
    print(small_dog.breed)
    print(medium_dog.age)